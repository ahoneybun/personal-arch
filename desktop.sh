userName="$1"

base_packages () {
    pacman -S unzip man-db tree git fish networkmanager iwd restic xdg-user-dirs --noconfirm
    xdg-user-dirs-update
}

gnome_install () {
    pacman -Syu
    pacman -S gnome --noconfirm
    pacman -S gnome-extra --noconfirm
    systemctl enable gdm
}

plasma_install () {
    pacman -Syu
    pacman -S plasma plasma-desktop plasma-meta plasma-wayland-session sddm packagekit-qt5 kde-system-meta kde-utilities-meta spectacle gwenview okular --noconfirm
    systemctl enable sddm

    # Set SDDM theme to Breeze
    mkdir /etc/sddm.conf.d/
    touch /etc/sddm.conf.d/theme.conf

    # Add values to this new file
    echo '[Theme]' | tee -a /etc/sddm.conf.d/theme.conf
    echo 'Current=breeze' | tee -a /etc/sddm.conf.d/theme.conf
}

cosmic_install () {
    pacman -Syu
    pacman -S cosmic --noconfirm
    systemctl enable cosmic-greeter
}

development () {
    pacman -S devtools rust rygel openssh dmidecode --noconfirm
}

virtual_machines () {
    pacman -S virt-manager edk2-ovmf dnsmasq qemu-full --noconfirm
}

fonts_install () {
    pacman -S ttf-indic-otf noto-fonts-cjk noto-fonts-emoji noto-fonts --noconfirm
}

media_creation () {
    pacman -S obs-studio inkscape --noconfirm
}

mozilla_install () {
    pacman -S firefox thunderbird --noconfirm
}

dot_files () {
    mkdir /home/$userName/.config
    # Setup SSH
    mkdir /home/$userName/.config/autostart/
    mkdir /home/$userName/.config/environment.d/
    
    # Download dot-files from GitLab
    curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kdeglobals > kdeglobals; mv kdeglobals /home/$userName/.config
    curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kglobalshortcutsrc > kglobalshortcutsrc; mv kglobalshortcutsrc /home/$userName/.config
    curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/krunnerrc > krunnerrc; mv krunnerrc /home/$userName/.config
    curl https://gitlab.com/ahoneybun/dot-files/-/raw/main/Plasma/kwinrc > kwinrc; mv kwinrc /home/$userName/.config

    # Fix directory ownership
    chown -R $userName:$userName /home/$userName/.config
}

user_config () {
    chsh --shell /usr/bin/fish $userName
    # Setting up Yubikey support
    echo 'KERNEL=="hidraw*", SUBSYSTEM=="hidraw", MODE="0664", GROUP="users", ATTRS{idVendor}=="2581", ATTRS{idProduct}=="f1d0"' | tee /etc/udev/rules.d/10-security-key.rules
    pacman -S libu2f-server pam-u2f --noconfirm
}

systemd_services () {
    systemctl enable pcscd
    systemctl enable NetworkManager
    systemctl enable iwd
    systemctl enable sshd
    # systemctl enable libvirtd
}

echo ""
echo "Which Desktop Environment do you want?"
echo "1) Plasma"
echo "2) GNOME"
echo "3) COSMIC"
echo "0) None or N/A"
read desktopChoice

if [ $desktopChoice = 1 ]; then
    base_packages
    plasma_install
    development
    dot_files
    user_config
    systemd_services
else

if [ $desktopChoice = 2 ]; then
    base_packages
    gnome_install
    development
    user_config
    systemd_services
else

if [ $desktopChoice = 3 ]; then
    base_packages
    cosmic_install
    user_config
    systemd_services

fi

fi
