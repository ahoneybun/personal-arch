# This file is used to configure the installed arch linux for daily use.

rootName="$1"

# Enable Pacman Parallel Downloads
sed -i 's/#ParallelDownloads/ParallelDownloads/g' /etc/pacman.conf

# Enable network time sync
timedatectl set-ntp true

# Set the timezone.
# Use `timedatectl list-timezones` and replace America/Denver with your country code.
timedatectl set-timezone America/Denver
ln -sf /usr/share/zoneinfo/America/Denver /etc/localtime

# Localization :

# 1. Generate '/etc/adjtime'.
hwclock --systohc

# 2. Set system locale :

# a. Uncomment required locales from '/etc/locale.gen'.
sed -i 's/#en_US.UTF-8/en_US.UTF-8/' /etc/locale.gen
# b. Generate locale.
locale-gen
# c. Set system locale. (creates locale.conf)
localectl set-locale LANG=en_US.UTF-8

# 3. set LANG variable system-wide.
echo 'export LANG=en_US.UTF-8' | tee -a /etc/profile

# Network configuration :

# 1. Create the hostname file.
echo "----------"
echo ""
echo "Specify hostname. This will be used to identify your machine on a network."
read hostName; echo $hostName > /etc/hostname

# 2. Add matching entries to '/etc/hosts'.
# If the system has a permanent IP address, it should be used instead of 127.0.1.1. 
echo -e 127.0.0.1'\t'localhost'\n'::1'\t\t'localhost'\n'127.0.1.1'\t'$hostName >> /etc/hosts

# Create regular user :

# 1. Add regular user.
echo "----------"
echo ""
echo "Specify username. This will be used to identify your account on this machine."
read userName;
useradd -m -G wheel -s /bin/bash $userName

# 2. Set password for new user.
echo "----------"
echo ""
echo "Specify password for regular user : $userName."; passwd $userName

# 3. Enable sudo for wheel group.
#sed -i 's/# %wheel ALL=(ALL:ALL) ALL/%wheel ALL=(ALL:ALL) ALL/g' /etc/sudoers
echo "$userName ALL=(ALL:ALL) ALL" | sudo EDITOR='tee -a' visudo

# Set the root password.
echo "----------"
echo ""
echo "Specify root password. This will be used to authorize root commands."
passwd

# Edit mkinitcpio hooks

echo "----------"
echo ""
echo "Did you enable Hibernation?"
echo "1) Yes"
echo "2) No"
echo -n "Enter choice: "; read hibState

case "$hibState" in
   1)
   sed -i 's/HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block filesystems fsck)/HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 btrfs filesystems resume fsck)/g' /etc/mkinitcpio.conf
   ;;
   2) 
   sed -i 's/HOOKS=(base udev autodetect microcode modconf kms keyboard keymap consolefont block filesystems fsck)/HOOKS=(base udev autodetect modconf kms keyboard keymap consolefont block encrypt lvm2 btrfs filesystems fsck)/g' /etc/mkinitcpio.conf
   ;;
 esac

# Create initramfs.
mkinitcpio -p linux

# Install systemd-boot bootloader :

# lsblk -f 

# 1. Install required packages.
pacman -S efibootmgr --noconfirm
bootctl install

# 2. Generate systemd-boot entry.
touch /boot/loader/entries/arch.conf
touch /boot/loader/entries/arch-lts.conf

if [ $hibState = 1 ]; then
   echo 'title Arch Linux' | tee -a /boot/loader/entries/arch.conf
   echo 'linux /vmlinuz-linux' | tee -a /boot/loader/entries/arch.conf
   echo 'initrd /initramfs-linux.img' | tee -a /boot/loader/entries/arch.conf
   echo "options cryptdevice=$rootName:crypt-root root=/dev/mapper/lvm-root resume=/dev/mapper/lvm-swap rootflags=subvol=@root rw" | tee -a /boot/loader/entries/arch.conf

   echo 'title Arch Linux LTS' | tee -a /boot/loader/entries/arch-lts.conf
   echo 'linux /vmlinuz-linux-lts' | tee -a /boot/loader/entries/arch-lts.conf
   echo 'initrd /initramfs-linux-lts.img' | tee -a /boot/loader/entries/arch-lts.conf
   echo "options cryptdevice=$rootName:crypt-root root=/dev/mapper/lvm-root resume=/dev/mapper/lvm-swap rootflags=subvol=@root rw" | tee -a /boot/loader/entries/arch-lts.conf

else

if [ $hibState = 2 ]; then
   echo 'title Arch Linux' | tee -a /boot/loader/entries/arch.conf
   echo 'linux /vmlinuz-linux' | tee -a /boot/loader/entries/arch.conf
   echo 'initrd /initramfs-linux.img' | tee -a /boot/loader/entries/arch.conf
   echo "options cryptdevice=$rootName:crypt-root root=/dev/mapper/lvm-root rootflags=subvol=@root rw" | tee -a /boot/loader/entries/arch.conf

   echo 'title Arch Linux LTS' | tee -a /boot/loader/entries/arch-lts.conf
   echo 'linux /vmlinuz-linux-lts' | tee -a /boot/loader/entries/arch-lts.conf
   echo 'initrd /initramfs-linux-lts.img' | tee -a /boot/loader/entries/arch-lts.conf
   echo "options cryptdevice=$rootName:crypt-root root=/dev/mapper/lvm-root rootflags=subvol=@root rw" | tee -a /boot/loader/entries/arch-lts.conf

fi

fi

# Set default editor.
echo 'export EDITOR=nano' | tee -a /etc/environment

echo ""
echo "1) AMD"
echo "2) Intel"
echo -n "Enter CPU: "; read $cpuName

case "$cpuName" in
   1) 
   pacman -S amd-ucode --noconfirm
   ;;
   2) 
   pacman -S intel-ucode --noconfirm
   ;;
   0)
   exit
   ;;
esac

curl https://gitlab.com/ahoneybun/arch-itect/-/raw/main/desktop.sh > desktop.sh; sh desktop.sh "$userName"
